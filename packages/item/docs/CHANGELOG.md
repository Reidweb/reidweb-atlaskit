# @atlaskit/item

## 3.1.1 (2017-08-24)

* bug fix; fix alignment of navigation item group action (issues closed: ak-3279) ([4f98025](https://bitbucket.org/atlassian/atlaskit/commits/4f98025))
## 3.1.0 (2017-08-22)

* feature; accept new ItemGroup.role prop (still defaults to "group") (issues closed: ak-3325) ([747d3da](https://bitbucket.org/atlassian/atlaskit/commits/747d3da))
## 3.0.0 (2017-08-18)

* bug fix; removing log command and moved some things to variables. ([0ec36f2](https://bitbucket.org/atlassian/atlaskit/commits/0ec36f2))
* feature; reverting back to the original Item theme, but adding before/after theming ([cc7da77](https://bitbucket.org/atlassian/atlaskit/commits/cc7da77))

* breaking; ContainerTitleDropdown rebuilt to use Item and Dropdown under the hood. No longer accepts a ([026ea83](https://bitbucket.org/atlassian/atlaskit/commits/026ea83))
* breaking; refactor of Item and Navigation to support Project Switcher dropdown menus ([026ea83](https://bitbucket.org/atlassian/atlaskit/commits/026ea83))
* feature; added new spacing prop for Items ([414757f](https://bitbucket.org/atlassian/atlaskit/commits/414757f))
## 2.2.3 (2017-08-18)

* bug fix; fix navigation group title rendering action button twice (issues closed: ak-3219) ([b82bc4c](https://bitbucket.org/atlassian/atlaskit/commits/b82bc4c))



## 2.2.2 (2017-08-11)

* bug fix; deprecating @atlaskit/drag-and-drop. It has been moved to react-natural-drag ([7183656](https://bitbucket.org/atlassian/atlaskit/commits/7183656))
## 2.2.1 (2017-08-07)

* bug fix; moving item spacing responsiblity from middle to before and after icons (issues closed: ak-3211) ([be80f99](https://bitbucket.org/atlassian/atlaskit/commits/be80f99))
## 2.2.0 (2017-08-03)

* feature; improving support and examples for drag and drop in navigation (issues closed: ak-1862) ([c1e0986](https://bitbucket.org/atlassian/atlaskit/commits/c1e0986))





## 2.1.0 (2017-07-28)


* fix; disable flex styling on item component to ensure expected height ([cdcada1](https://bitbucket.org/atlassian/atlaskit/commits/cdcada1))


* feature; add support for [@atlaskit](https://github.com/atlaskit)/drag-and-drop in [@atlaskit](https://github.com/atlaskit)/item ([8caee18](https://bitbucket.org/atlassian/atlaskit/commits/8caee18))

## 1.0.0 (2017-07-21)


* feature; generic item component to be composed into other components ([36ebd08](https://bitbucket.org/atlassian/atlaskit/commits/36ebd08))
