# @atlaskit/media-avatar-picker

## 2.3.0 (2017-08-11)

* feature; bump :allthethings: ([f4b1375](https://bitbucket.org/atlassian/atlaskit/commits/f4b1375))
## 2.2.4 (2017-08-03)

* bug fix; fixes broken storybooks due to ED-2389 ([184d93a](https://bitbucket.org/atlassian/atlaskit/commits/184d93a))

## 2.2.3 (2017-08-01)

* bug fix; bumping media-core ([6488cfc](https://bitbucket.org/atlassian/atlaskit/commits/6488cfc))




## 2.2.2 (2017-07-25)

## 2.2.1 (2017-07-21)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))
* fix; remove SC from peerDependencies to dependencies ([568161b](https://bitbucket.org/atlassian/atlaskit/commits/568161b))

## 2.2.0 (2017-07-10)


* feature; use [@atlaskit](https://github.com/atlaskit)/slider instead of the one from [@atlaskit](https://github.com/atlaskit)/media-avatar-picker ([10aee5d](https://bitbucket.org/atlassian/atlaskit/commits/10aee5d))

## 2.1.0 (2017-06-08)


* fix; remove media-test-helpers from avatar-picker src ([1c80d71](https://bitbucket.org/atlassian/atlaskit/commits/1c80d71))


* feature; add image uploader feature to avatar-picker ([d159185](https://bitbucket.org/atlassian/atlaskit/commits/d159185))
* feature; allow drag files into avatar-picker uploader ([0b5503f](https://bitbucket.org/atlassian/atlaskit/commits/0b5503f))

## 2.0.2 (2017-04-27)


* fix; update legal copy to be more clear. Not all modules include ADG license. ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 2.0.1 (2017-04-26)


* fix; update legal copy and fix broken links for component README on npm. New contribution and ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))

## 1.0.0 (2017-04-19)

## 1.0.0 (2017-04-19)

## 1.0.0 (2017-04-19)

## 1.0.0 (2017-04-19)

## 1.0.0 (2017-04-19)

## 1.0.0 (2017-04-19)

## 1.0.0 (2017-04-19)

## 1.0.0 (2017-04-19)

## 1.0.0 (2017-04-19)

## 1.0.0 (2017-04-19)


* feature; add export functionality to ImageNavigator component ([7c08a4e](https://bitbucket.org/atlassian/atlaskit/commits/7c08a4e))
